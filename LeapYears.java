package practice;

public class LeapYears {
	
	static boolean checkYear(int year) {
		// All years divisible by 400 ARE leap years
		if (year % 400 == 0)
			return true;

		// All year divide by 100 but year is not divisible by 400
		if ((year % 100 == 0) && (year % 400 != 0))
			return false;

		// All years divisible by 4 but not by 100 ARE leap years
		if ((year % 4 == 0) && (year % 100 != 0))
			return true;
		return false;
	}

	
	public static void main(String[] args) {

		System.out.println("Test case 1 ::");
		System.out.println(checkYear(2000) ? "2000 is Leap Year" : "2000 is Not a Leap Year");

		System.out.println("Test case 2 ::");

		System.out.println(checkYear(1700) ? "1700 is Leap Year" : "1700 is Not a Leap Year");
		System.out.println(checkYear(1800) ? "1800 is Leap Year" : "1800 is Not a Leap Year");
		System.out.println(checkYear(1900) ? "1900 is Leap Year" : "1900 is Not a Leap Year");
		System.out.println(checkYear(1900) ? "2100 is Leap Year" : "2100 is Not a Leap Year");

		System.out.println("Test case 3 ::");

		System.out.println(checkYear(2008) ? "2008 is Leap Year" : "2008 is Not a Leap Year");
		System.out.println(checkYear(2012) ? "2012 is Leap Year" : "2012 is Not a Leap Year");
		System.out.println(checkYear(2016) ? "2016 is Leap Year" : "2016 is Not a Leap Year");

		System.out.println("Test case 4 ::");

		System.out.println(checkYear(2017) ? "2017 is Leap Year" : "2017 is Not a Leap Year");
		System.out.println(checkYear(2018) ? "2018 is Leap Year" : "2018 is Not a Leap Year");
		System.out.println(checkYear(2019) ? "2019 is Leap Year" : "2019 is Not a Leap Year");

	}
}